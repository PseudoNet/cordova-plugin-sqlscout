package nz.net.pseudo.sqlscout;

import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;

import com.idescout.sql.SqlScoutServer;

import android.util.Log;
import android.content.Context;

public class SQLScout extends CordovaPlugin {
  private static final String TAG = "SQLScout";
  private SqlScoutServer sqlScoutServer;
  private Context context;

  public void initialize(CordovaInterface cordova, CordovaWebView webView) {
    super.initialize(cordova, webView);
    context = this.cordova.getActivity();

    Log.d(TAG, "Initializing SQLScout");
    sqlScoutServer = SqlScoutServer.create(context, context.getPackageName());

  }

  @Override
  public void onResume(boolean multitasking)  {
    sqlScoutServer.resume();
  }

  @Override
  public void onPause(boolean multitasking) {
    sqlScoutServer.pause();
  }

  @Override
  public void onDestroy() {
    sqlScoutServer.destroy();
  }

}
