# cordova-plugin-sqlscout

Allow access to Cordova Android SQLite Databases using the [SQLScout](http://www.idescout.com/) Intellij IDEA / Android Studio plugin 

## Usage
Install from git repo

`cordova plugin add https://gitlab.com/PseudoNet/cordova-plugin-sqlscout.git`

or from npm repo

`cordova plugin add cordova-plugin-sqlscout`


Install the SQLScout IDE plugin and follow usage instructions from [SQLScout](http://www.idescout.com/)
